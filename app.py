from starlette.applications import Starlette
from starlette.responses import JSONResponse
import gpt_2_simple as gpt2
import tensorflow as tf
import uvicorn
import os
import gc
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

app = Starlette(debug=False)

sess = gpt2.start_tf_sess(threads=1)
gpt2.load_gpt2(sess)

response_header = {
    'Access-Control-Allow-Origin': '*'
}

generate_count = 0


@app.route('/generate', methods=['GET'])
async def generate(request):
    global generate_count
    global sess

    params = request.query_params

    if params.get('api_key', '') != os.getenv('API_KEY'):
        return JSONResponse({'error': 'invalid api key', 'text': ''}, headers=response_header, status_code=403)

    prefix = params.get('prefix', '')[:500]
    text = gpt2.generate(sess,
                         length=int(params.get('length', 1023)) + len(prefix),
                         temperature=float(params.get('temperature', 0.7)),
                         top_k=int(params.get('top_k', 0)),
                         top_p=float(params.get('top_p', 0)),
                         prefix=prefix,
                         truncate=params.get('truncate', None),
                         include_prefix=str(params.get(
                             'include_prefix', True)).lower() == 'true',
                         return_as_list=True)[0]
    # TODO: Confirm I need both strip calls
    text = text.replace(prefix, '').strip().strip('\n')

    if '<|endoftext|>' in text:
        text = text.split('<|endoftext|>')[0]

    generate_count += 1
    if generate_count == 8:
        # Reload model to prevent Graph/Session from going OOM
        tf.reset_default_graph()
        sess.close()
        sess = gpt2.start_tf_sess(threads=1)
        gpt2.load_gpt2(sess)
        generate_count = 0
    gc.collect()

    return JSONResponse({'text': text}, headers=response_header)

if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=int(os.environ.get('PORT', 4242)))
